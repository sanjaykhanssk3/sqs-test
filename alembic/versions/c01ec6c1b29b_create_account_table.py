"""create account table

Revision ID: c01ec6c1b29b
Revises: 
Create Date: 2020-12-11 14:16:54.544606

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'c01ec6c1b29b'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    op.add_column(
        'student',
        sa.Column('queue_id', sa.String(100)),

    )


def downgrade():
    pass
