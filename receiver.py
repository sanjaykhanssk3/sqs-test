from main.classes.queue.sqs.receiver import Receiver
from main.classes.receiver.marks.main import Newmarks

receiver = Receiver(updateClass = Newmarks)
print("Listening for data")

while 1:
    receiver.receive()
