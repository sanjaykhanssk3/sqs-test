from main import orm
from datetime import datetime

from main.models.student.model import Student

base = orm.base()


class Marks(base):
    __tablename__ = 'marks'

    id = orm.column(orm.int , primary_key=True)
    student_id = orm.column(orm.fk(Student.id))
    updated_on = orm.column(orm.datetime , default = datetime.now)
    mark = orm.column(orm.str(100))

base.metadata.create_all(orm.engine)
