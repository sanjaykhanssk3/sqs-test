from main import orm
from datetime import datetime


base = orm.base()

class Student(base):
    __tablename__ = 'student'

    id = orm.column(orm.int , primary_key=True)
    name = orm.column(orm.int)
    updated_on = orm.column(orm.datetime , default = datetime.now)
    description = orm.column(orm.str(100))
    queue_id  = orm.column(orm.str(100))

    def upgrade(migrate_engine):

        base.metadata.bind = migrate_engine
        Student.__table__.update(migrate_engine)

base.metadata.create_all(orm.engine)
