import sqlalchemy as db
from sqlalchemy.ext.declarative import declarative_base
from .baseclass import BaseClass

class SQLAL(BaseClass):
    
    def __init__(self):
        super().__init__()

        self.engine = db.create_engine(f'{self.sql_type}://{self.user}:{self.password}@{self.host}/{self.db}')

        self.base = declarative_base
        
        self.column = db.Column
        self.int = db.Integer
        self.fk = db.ForeignKey
        self.str = db.String
        self.bool = db.Boolean
        self.datetime = db.DateTime
        self.realtaion = db.orm.relationship
        self.DBsession = db.orm.sessionmaker(bind=self.engine)      
        self.session = self.DBsession()