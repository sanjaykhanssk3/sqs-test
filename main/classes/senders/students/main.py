from main.models.student.model import Student
from main import orm
from main.classes.queue.sqs.send import Sender

"""Add new student and send data to sqs"""
class NewStudent:
    def __init__(self , name , description = None , mark =440):
        self.mark = mark
        self.new = Student(name = name, description = description)
        orm.session.add(self.new)
        orm.session.commit()
    
    def send_to_queue(self):
        sender = Sender()
        dct = {"id" :self.new.id , "mark" :self.mark}
        # body = f"""{id':'{self.new.id}' , 'mark':'{self.mark}'"""
        id = sender.send(str(dct))
        self.queue_id = id
        orm.session.commit()
        print("--"*30)