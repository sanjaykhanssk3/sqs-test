import boto3

from .config import Configure
# Get the service resource
class Sender(Configure):
    
    def __init__(self):
        super().__init__()

        


    def send(self , data):
        
        try:
            response = self.queue.send_message(
                MessageBody=data,
                # MessageGroupId='messageGroup1'
            )

            # The response is NOT a resource, but gives you a message ID and MD5
            # print(response.get('MessageId'))
            # print(response.get('MD5OfMessageBody'))
            return response.get("MessageId")
        except Exception as e:
            print(e)
