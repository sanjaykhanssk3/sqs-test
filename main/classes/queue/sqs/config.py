import json 
import os
import boto3
class Configure:
    def __init__(self):
        file = open( os.path.join(os.getcwd() , "main/classes/queue/sqs/key.json") )
        file_values = json.load(file)
        self.id = file_values["id"]
        self.key = file_values["key"]

        self.sqs = boto3.resource('sqs' , 
        region_name="us-east-1" ,
        aws_access_key_id = self.id,
        aws_secret_access_key = self.key)
        # url ="https://sqs.us-east-1.amazonaws.com/058648417882/testingQueue"
        # Get the queue
        self.queue = self.sqs.get_queue_by_name(QueueName="testQueue")
