import boto3
from .config import Configure

class Receiver(Configure):

    """Class to recive data from SQS"""

    def __init__(self , updateClass):
        super().__init__()
        self.updateClass = updateClass

    
    def receive(self):
        for message in self.queue.receive_messages():
            # print('Hello, {0}'.format(message.body))
            try:
                self.updateListener(message.body)
                print(message.body)
                message.delete()
                
                print("--"*20)
            except Exception as e:
                print( "Error:", e)
    
    def updateListener(self , data):
        """Update the class passed in the reciver constructor if message recieved"""
        self.updateClass().update(data)

        
